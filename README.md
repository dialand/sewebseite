Meine eigene Webseite zu erstellen, ist eine Projektarbeit im Rahmen 
des Accelerated Learning Kurses rund um Java gewesen, an dem ich teilnehme.
In diesem Projekt habe ich meine HTML/CSS und Bootstrap-
Kenntnisse vertieft. Ich habe dieses Thema gewählt, um von einer meinen Erfahrungen 
zu berichten, die ich während meiner Masterarbeit gemacht habe.

Die Webseite ist ein OnePager geworden und beleuchtet die Phase der Expertensuche näher, da ich stolz 
darauf bin, dass sich meine harte Arbeit ausgezahlt hat und ich 
viele Experten für Interviews zu meiner Forschungsarbeit gewinnen konnte.

